from django.urls import path
from .views import (
    home,
    show_article,
    display_viewed_articles,
    show_article_detail)

urlpatterns = [
    path('', home, name='home'),
    path('article/', show_article, name='show_article'),
    path('viewed_articles/', display_viewed_articles, name='viewed_articles'),
    path(
        'article_content/<int:id>/',
        show_article_detail,
        name='article_content'
    )
]
