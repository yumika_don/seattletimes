from django.apps import AppConfig


class ReadArticlesConfig(AppConfig):
    name = 'read_articles'

