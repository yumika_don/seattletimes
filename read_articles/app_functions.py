import requests
from bs4 import BeautifulSoup
from collections import namedtuple


def get_article_info(article_url):
    ''' Use requests library to retrieve article_url information.
        Modify User-Agent in headers to show that the request is made from
        a browser.

        :param article_url: a url string from seattletimes.com
        :type article_url: str
        :retrun: namedtuple with information of the article by using requests
        :rtype: namedtuple
     '''
    ArticleInfo = namedtuple('ArticleInfo', [
        'article_title',
        'article_content',
        'article_url',
        'paragraph_list'
    ])

    headers = {
         'User-Agent': 'User-Agent: Mozilla/5.0 \
        (Windows NT 10.0; Win64; x64; rv:86.0) Gecko/20100101 Firefox/86.0'
    }

    article_response = requests.get(article_url.strip(), headers=headers)
    response_code = article_response.status_code
    if response_code == 200:
        html = article_response.text
        soup = BeautifulSoup(html, 'lxml')
        article_title = soup.find(attrs={'class': 'article-title'})
        title_text = article_title.get_text().strip()
        article_content = soup.find(
            attrs={'class': 'article-content'}
        ).find_all('p')
        # article_content is a list of <p> tags
        p_tag_list = [
            p.get_text().strip() for p in article_content
        ]

        whole_string = ' '.join(p_tag_list)
        split_string = whole_string.split('.')
        to_be_saved_string = '.'.join(split_string)
        url_to_be_saved = article_url
    else:
        response_code.raise_for_status()

    article_info = ArticleInfo(
        article_title=title_text,
        article_content=to_be_saved_string,
        article_url=url_to_be_saved,
        paragraph_list=p_tag_list
    )
    return article_info
