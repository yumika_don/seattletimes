import arrow
from django.db import models
from django.utils import timezone


class ViewedArticles(models.Model):
    article_title = models.CharField(max_length=200)
    article_viewed_date = models.DateTimeField(default=timezone.now)
    article_content = models.TextField(null=True)
    article_url = models.URLField(blank=True)

    def __str__(self):
        return self.article_title[:50] + '...'

    @property
    def days_ago(self):
        today = arrow.utcnow()
        posted_days_ago = today - self.article_viewed_date
        total_secs = posted_days_ago.total_seconds()
        total_secs = str(total_secs).split('.')[0]
        total_secs = int(total_secs)
        humanized_duration = today.shift(seconds=-total_secs).humanize()
        return humanized_duration

    class Meta:
        ordering = ['-article_viewed_date']
