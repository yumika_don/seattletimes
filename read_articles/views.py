from .app_functions import get_article_info
from .models import ViewedArticles
from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from django.core.paginator import Paginator
from django.core.exceptions import ObjectDoesNotExist


def home(request):
    return render(request, 'home.html')


def show_article(request):
    ''' capture the url from request.GET, then use requests and
    BeautifulSoup to parse and display the url content '''

    seattletimes_url = request.GET['article_url'].strip()
    article_info = get_article_info(seattletimes_url)
    url_to_be_saved = article_info.article_url
    title_text = article_info.article_title
    to_be_saved_string = article_info.article_content
    content_to_display = article_info.paragraph_list

    # if the article is already in the database, just update its content,
    # its title and its added datetime
    try:
        url = ViewedArticles.objects.get(article_url=url_to_be_saved)
        url.article_title = title_text
        url.article_content = to_be_saved_string
        url.save()
    # if not, then add the article to the database as a new entry
    except ViewedArticles.DoesNotExist:
        article_to_be_saved = ViewedArticles(
            article_title=title_text,
            article_viewed_date=timezone.now(),
            article_content=to_be_saved_string,
            article_url=url_to_be_saved)
        article_to_be_saved.save()

    context = {
        'title_text': title_text,
        'paragraph_list': content_to_display
    }

    return render(request, 'article.html', context)


def display_viewed_articles(request):
    ''' get the list of already read articles from a database then display
    it in this view '''
    # get all viewed articles from DB, ordered by latest read articles
    all_viewed_articles = ViewedArticles.objects.order_by(
        '-article_viewed_date'
    )
    # add pagination - 10 articles per page
    paginator = Paginator(all_viewed_articles, 10)
    total_pages = paginator.num_pages
    page = request.GET.get('page')
    articles = paginator.get_page(page)

    context = {'viewed_article_list': articles,
               'total_articles': all_viewed_articles,
               'total_pages': total_pages}

    return render(request, 'viewed_articles.html', context)


def show_article_detail(request, id=None):
    ''' if an article is clicked on from display_viewed_articles, it
    will present a view of an article with its title and content '''
    article = get_object_or_404(ViewedArticles, id=id)
    context = {'article': article}

    return render(request, 'article_content.html', context)
